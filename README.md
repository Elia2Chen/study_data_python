# STUDY_DATA_PYTHON

## 介绍

数据结构PYTHON相关代码

## 相关文件说明

### LIST---线性表

文件名  | 注释
-------- | -----
sequence_list.py  | 顺序结构顺序表
single_link_list.py  | 单链表
static_linked_list.py  | 静态链表
circular_linked_list.py  |  循环链表
dobule_linked_list.py  |  双向循环链表

### Stack---栈

文件名  | 注释
-------- | -----
sequence_stack.py  | 顺序栈
sequence_share_stack.py  | 顺序栈:共享空间
single_stack.py  | 链式栈
arithmetic_stack.py | 四则运算栈

### Queue---队列

文件名  | 注释
-------- | -----
circular_sequence_queue.py  | 循环顺序队列
linked_queue.py  |  链式队列


