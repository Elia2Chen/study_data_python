"""
循环链表的实现:由于有头结点的存在，所以下标==位置
"""
from typing import Union


class Node(object):
  """
  链表 的 结点类
  """
  def __init__(self, data=None, next=None):
    """
    构造函数
    :param data:
    :param next:
    """
    self.data = data
    self.next = next


class CircularList(object):
  """
  循环链表
  """
  def __init__(self):
    self.head = Node()
    # 链表的结点个数
    self.length = 0
    # 初始化链表时 由于只有一个头结点 所以尾指针指向头指针
    self.rear = self.head
    self.rear.next = self.head

  def is_empty(self):
    return  self.length == 0

  def count(self):
    return self.length

  def __getitem__(self, index:int):
    """
    根据下标获取元素
    :param index:
    :return:
    """
    if not isinstance(index, int):
      raise TypeError
    if index <= 0 or index > self.length:
      raise IndexError
    p = self.head
    for i in range(index):
      p = p.next
    return p.data

  def append(self, value:Union[int, str]):
    """
    从链表尾部插入元素
    :param value:
    :return:
    """
    if self.is_empty():
      node = Node(data=value, next=None)
      self.head.next = node
      self.rear = node
      self.rear.next = self.head
    else:
      p = self.head
      while p.next != self.head:
        p = p.next
      node = Node(data=value, next=self.head)
      p.next = node
      self.rear = node
    self.length += 1

  def insert(self, index:int, value):
    """
    从链表任意位置插入元素:不会出现从尾部添加元素的情况
    :param index:
    :param value:
    :return:
    """
    if not isinstance(index, int):
      raise TypeError
    if index <= 0 or index > self.length:
      raise IndexError
    if self.is_empty():
      self.append(value)
    else:
      p = self.head
      for i in range(1, index):
        p = p.next
      node = Node(value)
      node.next = p.next
      p.next = node
    self.length += 1

  def find(self, value):
    """
    :param value:
    :return: index or -1
    """
    if self.is_empty():
      raise Exception('the circular linked list is empty')
    p = self.head.next
    i = 1
    while p != self.head:
      if p.data == value:
        return i
      i += 1
      p = p.next
    return -1

  def update(self, index:int, value):
    """
    :param index:
    :param value:
    :return:
    """
    if self.is_empty():
      raise Exception('the circular linked list is empty')
    if index <= 0 or index > self.length:
      raise IndexError
    if not isinstance(index, int):
      raise TypeError
    p = self.head
    for i in range(index):
      p = p.next
    p.data = value

  def delete(self, index:int):
    """
    delete 不会出现删除最尾部的情况 要删除最尾部 调用pop方法
    :param index:
    :return: item
    """
    if self.is_empty():
      raise Exception('the circular linked list is empty')
    if index <= 0 or index >= self.length:
      raise IndexError
    if not isinstance(index, int):
      raise TypeError
    p = self.head
    for i in range(1, index):
      p = p.next
    node = p.next
    p.next = p.next.next
    self.length -= 1
    return node

  def remove(self, value):
    """
    会出现删除最尾部的情况
    :param value:
    :return: -1 or item
    """
    if self.is_empty():
      raise Exception('the circular linked list is empty')
    p = self.head
    i = 0
    while p.next != self.head:
      if p.next.data == value:
        node = p.next
        if i == self.length:
          self.pop()
        else:
          p.next = p.next.next
          self.length -= 1
          return node
      p = p.next
      i += 1
    return -1

  def pop(self):
    """
    删除最尾部的元素
    :return: item
    """
    if self.is_empty():
      raise Exception('the circular linked list is empty')
    p = self.head
    for i in range(1, self.length):
      p = p.next
    item = self.rear
    self.rear = p
    self.rear.next = self.head
    self.length -= 1
    return item

  def data_list(self):
    """
    打印输出
    :return:
    """
    p = self.head
    while p.next != self.head:
      p = p.next
      print(p.data, end=' ')
    print()


def combine(circular_list_a: CircularList, circular_list_b:CircularList):
  """
  合并两个循环链表
  :param circular_list:
  :return:
  """
  if circular_list_a.is_empty():
    return circular_list_b
  elif circular_list_b.is_empty():
    return circular_list_a
  elif not circular_list_a.is_empty() and not circular_list_b.is_empty():
    p = circular_list_a.head
    circular_list_a.rear.next = circular_list_b.rear.next.next
    circular_list_b.rear.next = p
    return circular_list_a

if __name__=='__main__':
  s = CircularList()
  s.append(23)
  s.append(45)
  s.insert(2,48)
  s.data_list()

  sb = CircularList()

  sb.append(1)
  sb.append(2)
  sb.append(3)
  sb.data_list()

  ss = combine(s, sb)
  ss.data_list()
