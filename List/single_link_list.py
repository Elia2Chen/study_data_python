"""
单链表的实现: 此处的链表都带有头结点
index：均采用位置，所以在调用函数时 传位置 不传下标
"""
import random


class Node(object):
    """
    结点类
    """

    def __init__(self, data=None):
        """
        构造函数，可以当作头结点的初始化
        :param data: 结点的数据域
        """
        self.data = data
        self.next = None


class SingleLinkList(object):
    """
    链表类
    """

    def __init__(self, data=None):
        """
        构造函数
        """
        self.head = Node(data)  # 结点
        self.length = 0  # 单链表的长度

    def count(self):
        """
        单链表的长度
        :return: int
        """
        return self.length

    def is_empty(self):
        """
        判断单链表是否为空
        :return: bool
        """
        return self.head is None

    def __getitem__(self, index):
        """
        查找某个位置的元素是否存在
        :param index: 位置
        :return: 元素
        """
        if not isinstance(index, int):
            return TypeError
        if index < 0 or index >= self.length:
            return IndexError
        p = self.head.next
        for i in range(1, self.length+1):
            if i == index:
                return p
            p = p.next
        return '该位置元素不存在'

    def find(self, value):
        """
        查找某个元素
        :param value: 元素
        :return: 返回该元素所在的第一个下标 or -1
        """
        p = self.head.next
        i = 1
        while p is not None:
            if p.data == value:
                return i
            i += 1
            p = p.next
        return -1

    def append(self, value):
        """
        在单链表尾部插入元素
        :param value: 元素
        :return: 元素的位置
        """
        q = Node(value)
        if self.is_empty():
            self.head.next = q
        else:
            p = self.head
            while p.next is not None:
                p = p.next
            p.next = q
        self.length += 1

    def insert(self, index, value):
        """
        在单链表的某一位置插入元素
        :param index: 下标
        :param value: 元素数据域
        :return:
        """
        if not isinstance(index, int):
            raise TypeError
        if index < 0:
            raise IndexError
        if self.is_empty():
            self.append(value)
        else:
            node = Node(value)
            p = self.head.next
            for i in range(1, self.length):
                if i == index - 1:
                    node.next = p.next
                    p.next = node
                p = p.next
            self.length += 1

    def update(self, index, value):
        """
        修改某个位置的元素
        :param index:
        :param value:
        :return:
        """
        if not isinstance(index, int):
            raise TypeError
        if index < 0:
            raise IndexError
        p = self.head.next
        for i in range(1, self.length+1):
            if i == index:
                p.data = value
            p = p.next

    def remove(self, index):
        """
        删除某个位置的元素
        :param index: 位置
        :return: 元素数据
        """
        if not isinstance(index, int):
            raise TypeError
        if index < 0:
            raise IndexError
        p = self.head.next
        for i in range(1, self.length+1):
            if i == index - 1:
                q = p.next
                p.next = q.next
                self.length -= 1
                return p.data
            p = p.next

        return '没有找到该位置'

    def pop(self):
        """
        删除链表最后一个元素
        :return: 元素数据
        """
        p = self.head.next
        node = p
        while p.next is not None:
            node = p
            p = p.next
        q = p.data
        node.next = None
        self.length -= 1
        return q

    def __iter__(self):
        """
        遍历链表输出每个结点,包括头结点
        :return: node
        """
        p = self.head
        for i in range(self.length+1):
            print(p.data, end=' ')
            p = p.next
        print()

    def create(self, length):
        """
        单链表整体创建:目前初始化实例对象后 只有一个头结点
        :param length: 元素的个数
        :return: 返回链表的头指针
        """
        #   两种方法实现整体创建链表： 头插法 尾插法
        #   尾插法
        # p = self.head.next
        # for i in range(length):
        #     q = Node(data=random.randint(1,20))
        #     p.next = q
        #     p = q
        # p.next = None
        # 头插法
        p = self.head
        for i in range(length):
            q = Node(data=random.randint(1, 20))
            q.next = p.next
            p.next = q
        self.length = length
        return self.head

    def destroy(self):
        """
        单链表整体删除
        :return: bool
        """
        p = self.head.next
        for i in range(self.length):
            q = p.next
            del p
            p = q
        self.length = 0
        self.head = None
        return self.head


if __name__ == '__main__':
    s = SingleLinkList()  # 默认创建一个空的头结点
    s.create(3)
    s.__iter__()
    print('--------------')

    # s.update(2, 9)
    # s.__iter__()
    # print('--------------')

    # s.remove(2)
    # s.__iter__()
    # print('--------------')

    # s.insert(2,'s')
    # s.__iter__()
    # print('--------------')

    # print(s.__getitem__(2).data)
    # print('--------------')

    print(s.pop())
    s.__iter__()
    print('--------------')

    # print(s.find('s'))
    # s.__iter__()
    # print('--------------')

    s.append(12)
    s.__iter__()
    print('--------------')
    print(s.destroy())
