"""
双向循环链表的实现：带头结点的双向循环链表
"""

class Node(object):
  """
  结点类
  """
  def __init__(self, data=None, prior=None, next=None):
    """
    双向循环链表 前驱和后继都指向本身
    :param data:
    :param prior:
    :param next:
    """
    self.data = data
    self.prior = prior
    self.next = next


class DoubleLinkedList(object):
  """
  双向链表类
  """
  def __init__(self):
    self.head = Node()
    self.length = 0
    self.head.prior = self.head
    self.head.next = self.head

  def is_empty(self):
    return self.length == 0

  def count(self):
    return self.length

  def append(self, value):
    node = Node(value)
    p = self.head
    while p.next != self.head:
      p = p.next
    node.prior = p
    node.next = self.head
    p.next.prior = node
    p.next = node
    self.length += 1

  def insert(self, index:int, value):
    """
    :param index:
    :param value:
    :return:
    """
    if self.is_empty():
      raise Exception('the double linked list is empty')

    if not isinstance(index, int):
      raise TypeError

    if index <= 0 or index > self.length:
      raise IndexError
    p = self.head
    for i in range(1, index):
      p = p.next
    node = Node(value)
    node.prior = p
    node.next = p.next
    p.next.prior = node
    p.next = node
    self.length += 1

  def __getitem__(self, index:int):
    if self.is_empty():
      raise Exception('the double linked list is empty')

    if not isinstance(index, int):
      raise TypeError

    if index <= 0 or index > self.length:
      raise IndexError
    p = self.head.next
    for i in range(1, index):
      p = p.next
    return p

  def find(self, value):
    p = self.head.next
    index = 1
    while p != self.head:
      if p.data == value:
        return index
      p = p.next
      index += 1
    return -1

  def update(self, index:int, value):
    if self.is_empty():
      raise Exception('the double linked list is empty')

    if not isinstance(index, int):
      raise TypeError

    if index <= 0 or index > self.length:
      raise IndexError
    p = self.head
    for i in range(index):
      p = p.next
    p.data = value

  def delete(self, index:int):
    p = self.head
    for i in range(index):
      p = p.next
    p.prior.next = p.next
    p.next.prior = p.prior
    self.length -= 1
    return p

  def remove(self, value):
    p = self.head.next
    while p != self.head:
      if p.data == value:
        p.prior.next = p.next
        p.next.prior = p.prior
        self.length -= 1
        return p
      p = p.next
    return -1

  def pop(self):
    p = self.head.prior
    p.prior.next = p.next
    p.next.prior = p.prior
    self.length -= 1
    return p

  def data_list(self, des='Y'):
    if des == 'Y':
      # 正向输出
      p = self.head.next
      while p != self.head:
        print(p.data, end=' ')
        p = p.next
      print()
    if des == 'N':
      # 逆向输出
      p = self.head.prior
      while p != self.head:
        print(p.data, end=' ')
        p = p.prior
      print()


if __name__=='__main__':
  ds = DoubleLinkedList()
  ds.append(23)
  ds.append(34)
  ds.append(45)
  ds.insert(3,99)
  print(ds.count())
  ds.data_list()
  # ds.update(3,100)
  print(ds.delete(4).data)
  ds.data_list('N')
  ds.data_list()
