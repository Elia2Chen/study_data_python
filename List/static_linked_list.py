"""
静态链表的实现
index:指的是位置
"""


class Element(object):
  """
  数组中每个元素的类
  """

  def __init__(self, data=None, cur=0):
    """
    构造函数
    :param data: 数据域
    :param cur: 游标
    """
    self.data = data
    self.cur = cur


class StaticLinkedList(object):

  def __init__(self, maxsize=100):
    """实现构造函数"""
    self.maxsize = maxsize
    self.arr = [Element() for _ in range(self.maxsize)]
    # 不能写成 [Element()] * self.maxsize 不然相当于一个实例化的element复制了self.maxsize次，并不是每一个都是新的实例化对象
    self.arr[0].data = 0  # 链表的第一个结点的 数据域 用来记录 静态链表的长度
    # 需要 初始化所有备用链表的cur，不然更新备用链表的头结点时容易出现 None的情况
    for i in range(1, self.maxsize):
      self.arr[i-1].cur = i  # 链表的第一个结点的 游标 用来记录 备用链表的初始位置

  def is_empty(self):
    """判断是否为空"""
    return self.arr[0].data == 0

  def is_full(self):
    """判断是否满"""
    return self.arr[0].data >= self.maxsize

  def count(self):
    """返回静态链表的长度/静态链表的元素个数"""
    return self.arr[0].data

  def __getitem__(self, index):
    """
    查
    :param index: 位置
    :return: 元素内容
    """
    if not isinstance(index, int):
      raise TypeError
    if index <= 0 or index > self.arr[0].data:
      raise IndexError
    head_index = self.maxsize - 1
    for i in range(index):
      head_index = self.arr[head_index].cur  # 更新游标，遍历链表
    return self.arr[head_index].data

  def append(self, value):
    """
    从链表尾部添加数据, 需要更新备用链表的结点和 cur
    :param value: 数据
    :return:
    """
    free_index = self.arr[0].cur  # 获取备用链表的游标
    if self.is_empty():
      self.arr[free_index].data = value
      self.arr[self.maxsize - 1].cur = free_index  # 更新头结点的游标
      self.arr[0].cur += 1  # 更新备用链表的游标
    else:
      self.arr[free_index].data = value  # 将新增的元素放在备用链表
      self.arr[0].cur += 1  # 更新备用链表的游标
      head_index = self.arr[self.maxsize - 1].cur  # 获取头结点的游标
      for i in range(self.arr[0].data - 1):
        head_index = self.arr[head_index].cur  # 找到原链表 的 倒数第二个元素 获取最后一个元素的下标
      self.arr[head_index].cur = free_index  # 将最后一个元素的 游标 指向新增元素所在的下标
      self.arr[free_index].cur = 0  # 由于从尾部添加，所以 该元素的cur应该标识为0 表示他之后没有任何的元素了
    self.arr[0].data += 1  # 更新链表长度

  def insert(self, index, value):
    """
    从链表的某个下标插入值
    :param index: 下标
    :param value: 数据
    :return:
    """
    if not isinstance(index, int):
      raise TypeError
    if index <= 0:
      raise IndexError

    # 获取头结点的下标 !!!!
    head_index = self.maxsize - 1
    for i in range(index - 1):
      head_index = self.arr[head_index].cur  # 更新链表头结点
    # 到达更新位置:需要更新插入元素的上一个元素的游标 和 插入元素的游标
    # 获取备用链表的下标
    free_index = self.arr[0].cur
    self.arr[free_index].data = value
    self.arr[0].cur = self.arr[free_index].cur  # 此处更新空闲链表完毕
    # 更新 插入元素的游标
    self.arr[free_index].cur = self.arr[head_index].cur
    # 更新 插入元素上一个元素的游标
    self.arr[head_index].cur = free_index
    # 更新 链表长度
    self.arr[0].data += 1

  def update(self, index, value):
    """
    更新静态链表
    :param index: 位置
    :param value: 修改的值
    :return:
    """
    if not isinstance(index, int):
      raise TypeError
    if index <= 0 or index > self.arr[0].data:
      raise IndexError
    head = self.arr[self.maxsize - 1].cur
    for i in range(index - 1):
      head = self.arr[index].cur
    self.arr[head].data = value

  def delete(self, index):
    """
    删除静态链表某一下标的值
    更新备用链表时 需要被删除结点的下标
    更新数据链表时 需要被删除结点的上一个结点的下标
    且一定要先更新数据链表 再更新备用链表
    不然会造成更新数据链表时 拿不到后链的结点 导致断链
    :param index:
    :return: 返回被删除的结点
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index > self.arr[0].data:
      raise IndexError
    # 获取头结点下标
    head = self.maxsize - 1
    # 获取备用链表的下标
    free = self.arr[0].cur
    for i in range(index - 1):
      # 更新头结点 直到获取到要删除结点的上一个结点
      head = self.arr[head].cur
    # 要删除结点的下标
    del_index = self.arr[head].cur
    # 获取要删除的结点
    item = self.arr[del_index]
    # 获取要删除结点的下一个结点下标
    next_index = self.arr[del_index].cur
    # 更新数据链表
    self.arr[head].cur = next_index
    # 更新备用链表的头结点:先链后 再链前
    self.arr[del_index].cur = free
    self.arr[0].cur = del_index
    # 更新数据链表的长度
    self.arr[0].data -= 1
    return item

  def remove(self, value):
    """
    删除静态链表中某一值
    :param value:
    :return: 删除值 or -1
    """
    if self.is_empty():
      raise Exception('nothing to remove!')
    head = self.maxsize - 1 #
    for i in range(self.arr[0].data):
      if self.arr[self.arr[head].cur].data == value:
        # 此时的head指向的是要被删除结点的上一个结点下标
        # 获取 要删除结点的下标 和 对象
        del_index = self.arr[head].cur
        item = self.arr[del_index]
        # 获取 要删除结点的下一个下标
        next_index = self.arr[del_index].cur
        # 更新 数据链表
        self.arr[head].cur = next_index
        # 更新 备用链表：先后再前
        self.arr[del_index].cur = self.arr[0].cur
        self.arr[0].cur = del_index
        # 更新 数据链表长度
        self.arr[0].data -= 1
        return item
      head = self.arr[head].cur
    return -1

  def pop(self):
    """
    删除数据链表最后一个元素并返回
    :return: 元素
    """
    if self.is_empty():
      raise Exception('nothing to pop')
    index = self.maxsize - 1
    for i in range(self.arr[0].data - 1):
      index = self.arr[index].cur
    del_index = self.arr[index].cur
    item = self.arr[del_index]
    # 同删除逻辑 不再注释
    self.arr[index].cur = 0
    self.arr[del_index].cur = self.arr[0].cur
    self.arr[0].cur = del_index
    self.arr[0].data -= 1
    return item

  def find(self, value):
    """
    查找某个元素 并返回的是 在静态链表的真实下标
    比如 现 链表 长度为3 当你在位置2 插入元素45时
    实际上是在 下标4的位置 插入的45 返回的是4 而不是2 返回的是真实下标
    :param value: 元素
    :return: 下标 or -1
    """
    if self.is_empty():
      raise Exception('the list is empty')
    index = self.maxsize - 1
    for i in range(self.arr[0].data):
      if self.arr[index].data == value:
        return index
      index = self.arr[index].cur
    return -1

  def data_list(self):
    """
    输出静态链表
    :return:
    """
    index = self.maxsize - 1  # 获取头结点的游标
    for i in range(self.arr[0].data):
      index = self.arr[index].cur
      print(self.arr[index].data, end=' ')
    print()


if __name__ == '__main__':
  s = StaticLinkedList()
  s.append(23)
  s.append(34)
  s.append(92)
  s.data_list()
  # print(s.is_empty())
  # print(s.is_full())
  # print(s.count())
  # print(s.__getitem__(0))
  print('------------')
  s.insert(2, 45)
  s.data_list()
  item = s.find(45)
  print(s[4])
  s.data_list()
  print(s)
  del s