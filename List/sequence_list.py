"""
顺序表的实现:注意 main函数里面传的index都是下标值 所以需要将位置提前减1再放入使用
"""

class SequenceList(object):

  def __init__(self, MAXSIZE=4):
    self.num = 0  # 顺序表的元素个数
    self.length = MAXSIZE  # 顺序表的容量
    self.data = [None for _ in range(self.length)]

  def is_full(self):
    """
    判断 顺序表是否已满
    :return: bool
    """
    return self.num == self.length

  def is_empty(self):
    """
    判断 顺序表是否为空
    :return: bool
    """
    return self.num == 0

  def count(self):
    """
    返回顺序表元素个数
    :return: int
    """
    return self.num

  def __getitem__(self, index):
    """
    获取元素
    :param index: 请求位置
    :return: 元素
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index >= self.num:
      raise IndexError
    return self.data[index]

  def __setitem__(self, index, value):
    """
    修改元素
    :param index: 元素位置
    :param value: 元素的新值
    :return: 返回新值
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index >= self.num:
      raise IndexError
    self.data[index] = value
    return self.data[index]

  def insert(self, index, value):
    """
    在任意位置新增元素
    :param index: 选定的位置
    :param value: 新增的元素值
    :return: 返回新值
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index > self.num:
      raise IndexError
    if self.is_full():
      raise Exception('顺序表已满，无法添加数据')
    i = self.num
    while i > index:
      self.data[i] = self.data[i-1]
      i -= 1
    self.data[i] = value
    self.num += 1
    return self.data[index]

  def append_list(self, value):
    """
    在尾部位置新增元素
    :param value: 新增的元素值
    :return: 返回新值
    """
    if self.is_full():
      raise Exception('顺序表已满，无法添加数据')
    self.data[self.num] = value
    self.num += 1
    return self.data[self.num-1]

  def delete(self, index):
    """
    删除某一位置的元素
    :param index: 位置
    :return: 被删除的元素
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index >= self.num:
      raise IndexError
    element = self.data[index]
    i = index + 1
    while i < self.num:
      self.data[i-1] = self.data[i]
      i += 1
    self.num -= 1
    return element

  def pop(self):
    """
    删除最后一个位置的元素
    :return: 被删除的元素
    """
    element = self.data[self.num-1]
    self.num -= 1
    return element

  def findIndex(self, index):
    """
    根据位置查找元素
    :param index: 位置
    :return: 元素
    """
    if not isinstance(index, int):
      raise TypeError
    if index < 0 or index >= self.num:
      raise IndexError
    return self.data[index]

  def find(self, value):
    """
    找值
    :param value: 元素
    :return: 有，返回第一个存在的位置 否则返回-1
    """
    for i in range(self.num):
      if self.data[i] == value:
        return i + 1
    return -1

  def list_data(self):
    """
    输出data
    :return:
    """
    for i in range(self.num):
      print(self.data[i], end=' ')
    print()

  def destroy(self):
    """清表恢复原状"""
    self.__init__()


if __name__=='__main__':
  s = SequenceList()
  s.append_list(1)
  s.append_list(2)
  s.append_list(3)
  s.append_list(4)
  print(s.count())
  s.list_data()
  print('==========')
  print(s.delete(2))
  print(s.count())
  s.list_data()
  print('==========')
  print(s.insert(1, 5))
  print(s.count())
  s.list_data()
  s.append_list(9)