"""
链式队列
"""

class Node(object):
  def __init__(self, data=None):
    self.data = data
    self.next = None



class LinkedQueue(object):

  def __init__(self):
    node = Node()
    self.front = node
    self.rear = node
    self.count = 0


  def is_empty(self):
    return self.front == self.rear

  def push(self, value):
    node = Node(value)
    self.rear.next = node
    self.rear = node
    self.count += 1

  def pop(self):
    if self.is_empty():
      raise Exception('the queue is empty')
    item = self.front.next
    self.front.next = self.front.next.next
    self.count -= 1
    return item

  def queue_list(self):
    if self.is_empty() is False:
      p = self.front.next
      while p != None:
        print(p.data, end=' ')
        p = p.next
    print()


if __name__=='__main__':
  queue = LinkedQueue()
  print(queue.count)
  print(queue.is_empty())
  queue.queue_list()
  queue.push(12)
  queue.push(23)
  queue.push(34)
  print(queue.pop().data)
  print(queue.count)
  print(queue.is_empty())
  queue.queue_list()