"""
循环 顺序 队列
判定队满的时候 多留了一个空闲空间 即实际长度我们定为3 但只能入队两个元素
"""
class CircularLinkedQueue(object):

  def __init__(self, size=10):
    self.front = 0
    self.rear = 0
    self.size = size
    self.arr = [None for _ in range(self.size)]

  def count(self):
    count = (self.rear - self.front + self.size) % self.size
    return count

  def is_full(self):
    """
    判断队列是否满队
    :return:
    """
    return ( self.rear + 1) % self.size == self.front

  def is_empty(self):
    return self.rear == self.front

  def push(self, value):
    """
    入队
    :return:
    """
    if self.is_full():
      raise Exception('the queue is full')
    self.arr[self.rear] = value
    self.rear = (self.rear + 1) % self.size

  def pop(self):
    """
    出队
    :return:
    """
    if self.is_empty():
      raise Exception('the queue is empty')
    item = self.arr[self.front]
    self.front = (self.front + 1 ) % self.size
    return item

  def queue_list(self):
    head = self.front
    for i in range(self.count()):
      if self.arr[head] is not None:
        print(self.arr[head], end=' ')
      head = (head + 1) % self.size


if __name__=='__main__':
  queue = CircularLinkedQueue(size=3)
  queue.push(23)
  queue.push(45)
  # queue.push(98)
  queue.queue_list()