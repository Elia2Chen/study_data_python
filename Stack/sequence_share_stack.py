"""
顺序栈：两个栈共享空间
"""
from typing import Union

class SequenceShareStack(object):

  def __init__(self, size=10):
    self.size = size
    self.arr = [None for _ in range(self.size)]
    self.top1 = -1
    self.top2 = self.size

  def is_empty(self, stacknumber:Union[int, str]=''):
    """
    判断是否为空栈
    :param stacknumber:
    :return:
    """
    if stacknumber == 1:
      return self.top1 == -1
    elif stacknumber == 2:
      return self.top2 == self.size
    elif stacknumber == '':
      return self.top1 == -1 and self.top2 == self.size
    else:
      return Exception('the stacknumber is error')

  def is_full(self, stacknumber:Union[int, str]=''):
    """
    判断是否满栈
    :param stacknumber:
    :return:
    """
    if stacknumber == 1:
      return self.top1 == self.size - 1
    elif stacknumber == 2:
      return self.top2 == 0
    elif stacknumber == '':
      return self.top1 + 1 == self.top2
    else:
      return Exception('the stacknumber is error')

  def count(self, stacknumber:int):
    """
    判断各个栈的数量
    :param stacknumber:
    :return:
    """
    if stacknumber == 1:
      return self.top1 + 1
    elif stacknumber == 2:
      return self.size - self.top2
    else:
      return Exception('the stacknumber is error')

  def push(self, stacknumber:Union[int, str], value):
    """
    入栈
    :param stacknumber:
    :param value:
    :return:
    """
    if self.is_full():
      raise Exception('the stack is full')
    if stacknumber == 1:
      self.arr[self.top1 + 1] = value
      self.top1 += 1
    elif stacknumber == 2:
      self.arr[self.top2 - 1] = value
      self.top2 -= 1
    else:
      return Exception('the stacknumber is error')

  def pop(self, stacknumber:int):
    """
    弹栈
    :param stacknumber:
    :return:
    """
    if self.is_empty(stacknumber):
      raise Exception('the stacknumber:'+ str(stacknumber)+ ' is empty')
    if stacknumber == 1:
      item = self.arr[self.top1]
      self.arr[self.top1] = None
      self.top1 -= 1
    elif stacknumber == 2:
      item = self.arr[self.top2]
      self.arr[self.top2] = None
      self.top2 += 1
    else:
      return Exception('the stacknumber is error')
    return item

  def stack_list(self, stacknumber:int):
    """
    遍历 应该是从栈底到栈顶
    :param stacknumber:
    :return:
    """
    if stacknumber == 1:
      for i in range(self.top1+1):
        print(self.arr[i], end=' ')
    elif stacknumber == 2:
      for i in range(self.size-1, self.top2-1, -1):
        print(self.arr[i], end=' ')
    else:
      raise Exception('the stacknumber is error')
    print()

  def clear(self, stacknumber:Union[int, str]=''):
    """
    清空栈
    :param stacknumber:
    :return:
    """
    if stacknumber == 1 or stacknumber == '':
      for i in range(self.top1, -1, -1):
        del self.arr[i]
        self.top1 -= 1
    elif stacknumber == 2 or stacknumber == '':
      print(self.top2, self.size)
      for i in range(self.top2, self.size):
        del self.arr[i]
        self.top2 += 1
    else:
      raise Exception('the stacknumber is error')


  def get_top(self, stacknumber:int):
    """
    获取栈顶元素
    :param stacknumber:
    :return:
    """
    if self.is_empty(stacknumber):
      raise Exception('the stacknumber:'+ str(stacknumber)+ ' is empty')
    if stacknumber == 1 :
      return self.arr[self.top1]
    elif stacknumber == 2 :
      return self.arr[self.top2]
    else:
      raise Exception('the stacknumber is error')



if __name__=='__main__':
  stack = SequenceShareStack(size=3)

  stack.push(1, 1)
  stack.push(1, 2)
  stack.push(2, 3)
  stack.pop(1)
  stack.push(2, 4)
  stack.stack_list(1)
  # stack.clear(1)
  stack.stack_list(2)
  stack.clear(2)
  print(stack.is_full())
