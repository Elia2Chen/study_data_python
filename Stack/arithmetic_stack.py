"""
普通四则运算表达式（中缀表示法）转为 后缀表示法
后缀表示法计算表达式的结果
"""

from sequence_stack import SequenceStack


class Arithmetic(object):
  """
  中缀表达式类
  """
  def __init__(self, formula:str):
    """
    将中缀表达法 转换为 后缀表达法
    利用顺序栈来对 算式里面的符号进行进出栈操作
    :param formula:
    """
    self.formula_list = formula.split(' ')
    if len(self.formula_list) < 3:
      raise Exception("it isn't a standard formula.")
    self.size = int(len(self.formula_list) / 2) + 1
    self.stack = SequenceStack(size=self.size)
    self.data = self.turn_data()
    self.result = self.get_result(self.data)

  def turn_data(self):
    """
    return self.data
    :return: str
    """
    data = ''
    left = ['(', '{', '[']
    right = [')', '}', ']']
    for s in self.formula_list:
      if s.isdigit():
        data = data + s + ' '

      elif s == '+' or s == '-':
        # 四则运算 当栈顶元素等级大于等于 当前元素等级  栈内元素出栈直到有符号等级小于当前运算符等级，然后当前运算符进栈
        if self.stack.is_empty() is False and self.stack.getTop() not in left and self.stack.getTop() not in right:
          # * / + -
          for i in range(self.stack.count()):
            data = data + self.stack.pop() + ' '
        self.stack.push(s)

      elif s in left:
        self.stack.push(s)

      elif s in right:
        for i in range(self.stack.count()):
          item = self.stack.pop()
          if item in left:
            break
          else:
            data = data + item + ' '

      elif s == '*' or s == '/':
        # 四则运算 当栈顶元素等级大于等于 当前元素等级  栈内元素出栈直到有符号等级小于当前运算符等级，然后当前运算符进栈
        if self.stack.is_empty() is False and (self.stack.getTop() == '*' or self.stack.getTop() == '/'):
          for i in range(self.stack.count()):
            if self.stack.getTop() != '+' or self.stack.getTop() != '/':
              data = data + self.stack.pop() + ' '
            else:
              break
        self.stack.push(s)

    if self.stack.is_empty() is False:
      for i in range(self.stack.count()):
        data = data + self.stack.pop() + ' '
    return data


  def get_result(self, data):
    """
    通过后缀表示法计算结果值
    :return: int
    """
    result = 0
    for s in data.split(' '):
      if s.isdigit():
        self.stack.push(int(s))
      else:
        if self.stack.count() >= 2:
          # 作业数/运算符右边的数
          number_do = self.stack.pop()
          # 被作业数/运算符左边的数
          number_to_do = self.stack.pop()
          if s == '+':
            result = number_to_do + number_do
          elif s == '-':
            result = number_to_do - number_do
          elif s == '*':
            result = number_to_do * number_do
          elif s == '/':
            result = number_to_do / number_do
        self.stack.push(result)
    if self.stack.count() == 1:
      result = self.stack.pop()
    return result





if __name__ == '__main__':
  a = Arithmetic(formula='9 + ( 3 - 1 ) * 3 + 10 / 2')
  print(a.data)
  print(a.result)


