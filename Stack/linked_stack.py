"""
链栈：类似于单链表
"""


class Node(object):
  """
  结点类
  """

  def __init__(self, data):
    self.data = data
    self.next = None


class LinkedStack(object):
  """
  链栈
  """

  def __init__(self):
    self.top = None  # 相当于单链表中的head指针用处
    self.count = 0

  def is_empty(self) -> bool:
    return self.top is None

  def count(self) -> int:
    return self.count

  def push(self, value):
    """
    压栈
    :param value:
    :return:
    """
    node = Node(value)
    node.next = self.top
    self.top = node
    self.count += 1

  def pop(self):
    """
    弹栈
    :return:
    """
    if self.is_empty():
      raise Exception('the stack is empty')
    item = self.top
    self.top = self.top.next
    self.count -= 1
    return item

  def get_top(self):
    """
    返回栈顶元素
    :return:
    """
    return self.top

  def clear(self):
    """
    清空
    :return:
    """
    p = self.top
    while p != None:
      self.top = p.next
      del p
      p = self.top
      self.count -= 1

  def stack_list(self):
    p = self.top
    while p != None:
      print(p.data, end=' ')
      p = p.next


if __name__ == '__main__':
  stack = LinkedStack()
  stack.push(1)
  stack.push(2)
  stack.push(3)
  print(stack.get_top().data)
  stack.clear()
  stack.stack_list()
  del stack