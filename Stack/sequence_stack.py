"""
顺序栈
"""

class SequenceStack(object):
  """
  顺序栈的结构
  """
  def __init__(self, size=10):
    self.top = -1  # 初始顶部为-1 标识空栈
    self.size = size  # 栈的大小
    self.arr = [None for _ in range(self.size)]

  def is_empty(self):
    return self.top == -1

  def is_full(self):
    return self.top + 1 == self.size

  def count(self):
    return self.top + 1

  def push(self, value):
    if self.is_full():
      raise Exception('the stack is full')
    self.arr[self.top+1] = value
    self.top += 1

  def pop(self):
    if self.is_empty():
      raise Exception('the stack is empty')
    item = self.arr[self.top]
    self.arr[self.top] = None
    self.top -= 1
    return item

  def clear(self):
    """
    清空栈
    :return:
    """
    for i in range(self.top+1):
      self.pop()

  def getTop(self):
    """
    返回栈顶元素
    :return:
    """
    if self.is_empty():
      raise Exception('the stack is empty')
    return self.arr[self.top]

  def stack_list(self):
    for i in range(self.top+1):
      print(self.arr[i], end=' ')
    print()



if __name__ == '__main__':
  stack = SequenceStack()
  stack.push(23)
  stack.push(34)
  stack.stack_list()
  print(stack.getTop())
  # print(stack.pop())
  # stack.stack_list()
  del stack